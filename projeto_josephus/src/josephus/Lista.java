/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package josephus;

/**
 *
 * @author andreBolzan
 */
public interface Lista<E> {
    /**
     * Adiciona um elemento na lista em ordem alfabetica. 
     * @param e Elemento a ser inserido 
     * @return True caso a operacao foi realizada com sucesso e 
     * False caso contrario.
     */
    public boolean add(E e);
    
    /**
     * Remove todos os elementos da lista.
     */
    public void clear();
    
    /**
     * Verifica se um elemento existe na lista.
     * @param e Elemento a ser procurado.
     * @return True caso elemento exista na lista e 
     * False caso contrario.
     */
    public boolean contains(E e);
    
    /**
     * Retorna o elemento no indice informado.
     * @param index indice da lista.
     * @return O elemento no dado �ndice ou NULL caso 
     * o indice nao exista. 
     */
    public E get(int index);
    
    /**
     * Retorna o indice onde o elemento informado esta armazenado.
     * @param e Elemento a ser procurado.
     * @return O indice caso o elemento exista ou -1 caso contrario.
     */
    public int indexOf(E e);
   
    /**
     * Verifica se a lista � vazia.
     * @return True caso a lista � vazia ou False caso contr�rio.
     */
    public boolean isEmpty();
    
    /**
     * Remove um elemento da lista.
     * @param e Elemento a ser removido.
     * @return True caso a opera��o foi realizada com sucesso e 
     * False caso o elemento n�o exista na lista.
     */
    public boolean remove(E e);
    
    /**
     * Armazena o elemento informado no �ndice informado.
     * @param index �ndice da lista.
     * @param e Novo elemento a ser armazendo.
     * @return Retorno o elemento atualmente armazenado ou ]
     * null caso o �ndice n�o exista.
     */
    public E set(int index, E e);
   
    /**
     * Retorna o n�mero de elementos armazenados na lista.
     * @return 
     */
    public int size();
    
    /**
     * Retorna o �ndice m posi��es � esquerda a partir do �ndice indicado por in�cio.
     * @param inicio �ndice em que deve ser iniciado a busca.  
     * @param m N�mero de posi��es � esquerda
     * @return �ndice resultante ou -1 caso a lista esteja vazia ou
     * o indice de inicio seja maior que o n�mero de elementos na lista.
     */
    public int indexOfLeft(int inicio, int m);

    /**
     * Retorna o �ndice m posi��es � direita a partir do �ndice indicado por in�cio.
     * @param inicio �ndice em que deve ser iniciado a busca.  
     * @param m N�mero de posi��es � direita
     * @return �ndice resultante ou -1 caso a lista esteja vazia ou 
     * o indice de inicio seja maior que o n�mero de elementos na lista.
     */
    public int indexOfRigth(int inicio, int m);
}
