/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package josephus;

/**
 *
 * @author andreBolzan
 */
public class No {
    
    private Object info;
    private No prox;
    private No ant;

    public No(Object info) {
        this.info = info;
        this.prox = null;
        this.ant = null;
    }

    public Object getInfo() {
        return info;
    }

    public No getProx() {
        return prox;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }

    public No getAnt() {
        return ant;
    }

    public void setAnt(No ant) {
        this.ant = ant;
    }    
}
