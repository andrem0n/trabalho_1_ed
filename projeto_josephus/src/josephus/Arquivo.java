/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package josephus;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

/**
 *
 * @author andreBolzan
 */
public class Arquivo {
    private static boolean DEBUG = false;
    private int m; //incremento
    private String[] nomes; //vetor que armazena o nome das pessoas
    
    public Arquivo(String nomeArquivo){ 
        inicializa(nomeArquivo);
    }
    
    private void inicializa(String nomeArquivo){
        String line;
        StringTokenizer tokens; 
        int n;
        
        if(DEBUG)
            System.out.println("Arquivo: "+nomeArquivo);
        
        try {
            FileInputStream inFile = new FileInputStream(nomeArquivo);
            BufferedReader buff = new BufferedReader(new InputStreamReader(inFile, "UTF-8"));
            line = buff.readLine();
            if (line != null) {
                if(DEBUG)
                    System.out.println("line = "+ line);
                
                tokens = new StringTokenizer(line, " ");
                n = Integer.valueOf(tokens.nextToken());
                this.m = Integer.valueOf(tokens.nextToken());
                
                nomes = new String[n];
                for(int i=0; i<n; i++){
                    line = buff.readLine();
                    if(line == null)
                        break;
                    nomes[i] = line.trim();
                }
            }
            buff.close();
            inFile.close();
        }//end try
        catch (FileNotFoundException f) {
            JOptionPane.showMessageDialog(null, "Arquivo " + nomeArquivo + " não encontrado.\n" + f.getMessage(), "Output error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
         } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error message:", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    
    /**
     * Retorna o incremento do algoritmo informado no arquivo.
     * @return Incremento.
     */
    public int getIncremento() { return m;}
    
    /**
     * Retorna o nome das pessoas informado no arquivo.
     * @return Vetor com o nome das pessoas.
     */
    public String[] getNomes() {return nomes;}
    
}
